# My software kit for writing research papers

*04/10/2023*

## Writing text

### Markdown

[Markdown](https://www.markdownguide.org/getting-started/) is a lightweight markup language that you can use to add formatting elements to plain text documents. Created by John Gruber in 2004, Markdown is now one of the world’s most popular markup languages.

There are few formatting commands in the basic syntax, all are described [here](https://www.markdownguide.org/basic-syntax/).

Basic Markdown formatting commands are:

    # Heading level 1
    ## Heading level 2
    ### Heading level 3
    #### Heading level 4
    ##### Heading level 5
    ###### Heading level 6
    
    **Bold**

    _Italic_

    Lists
    - Item
    - Item
    - Item

    Numbered lists:
    1. Item
    2. Item
    3. Item

    > Quotes

    `Inline code`

    ```
    Code fences
    ```

To create paragraphs, use a blank line to separate one or more lines of text.

To create a line break or new line (`<br>`), end a line with two or more spaces, and then type return.

### Text editor

Writing in Markdown requires a text editor; most editors will have Markdown support, either built-in or available as a plugin. My personal favorite is [Sublime Text](https://www.sublimetext.com/) -- while it's not freeware, it's an excellent writing/coding editor and worth every of $99 USD.

I recommend using the following Sublime Text plugins:

- [Markdown Editing](https://github.com/SublimeText-Markdown/MarkdownEditing) -- Markdown syntax highlighting
- [Markdown Images](https://github.com/xsleonard/sublime-MarkdownImages) --  render images inside of Markdown files
- [Bracket Highlighter](https://github.com/facelessuser/BracketHighlighter) -- highlights bracket pairs
- [Buffer Scroll](https://github.com/titoBouzout/BufferScroll) -- saves/restores the last editing position
- [Table Editor](https://github.com/SublimeText-Markdown/TableEditor) -- for editing tables
- [Cite Bibtex](https://github.com/sjpfenninger/citebibtex) -- for quick inserting citations from the .bib files
- [Unicode Math](https://github.com/mvoidex/UnicodeMath) -- for inserting Unicode math symbols

### Links, tables and images

URL links are added using the `[name of the link](URL address)`.

Images are placed in the text using the `![alternative_text](path_to_image)` command.

Tables are part of the extended syntax and are described [here](https://www.markdownguide.org/extended-syntax/). The easiest way to create and edit Markdown tables is using the [Table Editor](https://github.com/SublimeText-Markdown/TableEditor) plugin.

## References

[JabRef](https://www.jabref.org/) is an open-source, cross-platform citation and reference management software. It is used to collect, organize and search bibliographic information. Jabref version 5.11 supports pushing citations to SublimeText and can be configured for Markdown/Pandoc-compatible citations (`[@citation_key`).  

## Figures and illustrations

Inkscape
Gimp
dotviz

## Final processing

Pandoc

LibreOffice