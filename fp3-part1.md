# Fairphone 3+ and /e/, part 1

*23/10/2020*

Today I have replaced my iPhone SE with Fairphone 3+ ([FP3+](https://shop.fairphone.com/en/fairphone-3-plus)) running [/e/](https://e.foundation/) (degoogled Android). Here are some of my initial impressions.

The FP3+ phone is big. Must get used to its size, as its significantly larger than my old phone (I've always had medium sized phones). I fits in my pockets easily, though. The fingerprint sensor is reachable without any problems. The very first impression is good: it's a well-made and solid gear. It's responsive (subjectively it's not slower than my iPhone SE) and its display is very good. And it has a headphone jack. And removable battery. Man, I like it.

I've order mine FP3+ from the e Foundation. I could buy it directly from the Fairphone company (as the installation of /e/ on Fairphones is an easy and well documented process), but I wanted to support the e Foundation financially this way.

I was very positively surprised how polished the /e/ system is. I was a bit worried about some apps I need daily (banking and Sygic GPS). Although one very friendly guy from the /e/ forum confirmed that these should work without any problem, it was a bit risky decision as I couldn't test these in advance. To make a long story short, everything works as expected. In the /e/ app store I could find almost all apps that I need. The only exception was an exotic control app for my music system (Linn Kazoo). ~~Will solve this problem later.~~ UPDATE: Linn Kazoo has been added to the app store after my request.

I'll write some more impression later. As for today, I think that I won't be regretting this switch. Well done Fairphone and e Foundation!