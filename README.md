# About me

- [professor of psychiatry](https://search.cit.umed.pl/en/department-details/deptcode/KL32) ([my CV](https://notabug.org/AdamWysokinski/FreeMind/src/master/aw_cv.pdf)) at the Department of Old Age Psychiatry and Psychotic Disorders, Medical University of Lodz, Poland
- [researcher](https://www.researchgate.net/profile/Adam-Wysokinski) in schizophrenia and neuromodulation

## My other activities

- [photography](https://adamwysokinski.aminus3.com)
- [dark ambient and techno](https://black-sun.bandcamp.com)
- [visual arts](https://www.deviantart.com/black---sun)

I'm also the free software enthusiast:
- [Debian](https://www.debian.org) GNU/Linux
- [Julia](https://julialang.org/)

I like: badgers, black coffee, Legendary Pink Dots, King Crimson, Basic Channel, Biosphere, Twin Peaks, vege ramen, Aikido (1st kyū), Stanisław Lem, Philip K. Dick, [movies](https://www.filmweb.pl/user/AdamWysokinski/films), mountaineering and winter trekking, long distance and trail running and bikepacking.

I'm anti-corporate and dislike privacy invading companies, therefore I've replaced Google with [DuckDuckGo](https://duckduckgo.com), Twitter with [Mastodon](https://mastodon.social), Dropbox with [Sync](https://www.sync.com) and avoid Facebook. Also, I also encourage you to start using Firefox with uBlock, uMatrix and NoScript plugins.

## My setup

### Research equipment

- transcranial electric stimulator: DC-STIMULATOR PLUS (neuroCare, Germany)
- transcranial magnetic stimulator: DuoMAG XT-100 (DEYMED Diagnostic, Czech Republic)
- SimplEEG_42 (ELMIKO, Poland)
- CANTAB Cognitive Research system (Cambridge Cognition, UK)
- bioimpedance analyzer: Maltron BIOSCAN 920-2-S Body Fat Analyzer (Maltron, UK)

### Hardware

- main workstation: AMD Threadripper 3960X, ASUS Prime TRX40-Pro, RAM: 128 GB, NVIDIA Quadro P2200, SSD: Samsung NVMe (/), HDDs: WD Black (~), WD Red Plus (backup), Eizo EV2456 display, Logitech peripherals
- laptop: Thinkpad T14 Ryzen 5 Pro, Philips 273V display, Logitech peripherals
- Raspberry 4B
- mobile: [Fairphone 3+](https://www.fairphone.com/en/) running [/e/](https://e.foundation/)

### Software

- Debian GNU/Linux (Testing) -- on my workstation, laptop and RPi4
- Xorg + i3 windows manager
- Firefox
- Sublime Text -- still the best text editor I know (ST replaced Emacs, which replaced Neovim, which replace Vim, which replaced BBEdit..)
- Markdown + JabRef
- [Julia](https://julialang.org) -- my favorite programming language so far and my daily driver for statistical analysis, EEG processing, machine learning
- Python -- mostly EEG processing using [MNE](https://mne.tools/stable/index.html) and machine learning, replaced with Julia
- R -- for statistical analysis, mostly replaced with Julia
- MATLAB (for EEGLAB, FieldTrip, SPM, Chronux, Brainstorm, Roast and NBT) -- replaced with Julia
- Stata (replaced with Julia and R)
- GIMP (replaced Affinity Photo)
- Inkscape (replaced Affinity Design)
- RawTherapee (replaced CaptureOne), Hugin, Siril, Luminance HDR
- Mandelbulber, JWildfire
- [Bitwig Studio](https://www.bitwig.com/)

## Contact

How you can reach me:
- [private e-mail](mailto:adam.wysokinski@tutanota.com)
- [work e-mail](mailto:adam.wysokinski@umed.lodz.pl)
- [Mastodon](https://fediscience.org/@adam_wysokinski)

My public GPG/PGP [key](https://notabug.org/AdamWysokinski/FreeMind/src/master/public.gpg) (fingerprint: 7326 DD49 A48D 1A8A BC3D B3B5 326F CC41 624E EDB7)
