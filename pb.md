# PocketBook Basic Lux 3

*18/10/2022*

In the process of replacing my devices with (preferably Linux-based) non-mainstream alternatives, this month I have replaced my worn-out Kindle 5 (the old one, not Paperwhite). My new e-book reader is [PocketBook Basic Lux 3](https://www.pocketbook-int.com/us/news/pocketbook-basic-lux-3-more-possibilities-more-comfort) (PBL3). This will be my initial remarks regarding the device.

TL;DR it's a great e-book reader.

The PocketBook company was found in Kiev, Ukraine (and later moved to Switzerland). This was one of the arguments for the device, as I did not wanted to support Amazon corporation anymore. While my Kindle 5 experience was generally fine, I wanted a new reader with a front-light -- I read a lot while trekking (while sleeping in a tent) and using a headlamp for reading makes reading very difficult due to light reflected from the screen, also I need to save headlamp batteries. Also, I wanted physical button as, again while trekking, I often sleep in cold temperatures, with my powerstrech gloves on. The lack of physical button makes turning pages difficult, if not impossible without taking gloves off. Also, I wanted a reader that support EPUB format. Although I manage my e-books using [Calibre](https://calibre-ebook.com/), which is a superb piece of software) and there are no problems with converting EPUB to MOBI, I wanted a more universal system, capable of opening various file formats (PocketBook supports EPUB, DJVU, DOC, FB2, HTML, MOBI, RTF, TXT, JPEG, BMP, PNG and TIFF, as well as DRM-protected EPUB and PDF files). I don't need audio-books (and therefore Bluetooth) support. PBL3 ticks all these marks. Moreover, as a bonus, it runs Linux and allows easy installation of applications, e.g. [KOReader](https://github.com/koreader/koreader/wiki/Installation-on-PocketBook-devices).

While it's physical dimensions are slightly smaller than my old Kindle 5, the screen of similar size. Build quality is very good, materials are of good quality; all parts are fitted precisely and tightly. PBL3 boots slightly longer than Kindle 5, waking of from the suspend is immediate. Both the system and e-book reading app are very responsive. Apart from its front-light (which works very good), it also offers SMARTlight technology. Warmer light is very comfortable for night reading, also it's less disturbing to your sleeping partner (especially when you sleep together in limited space of a small tent). Reading preferences are standard (font, size, line spacing, margins, etc.). If one needs more detailed customization, the KOReader offers a tremendous amount of customizations (also via additional plugins). As it's an overkill for my needs, I stayed with the included reading application. I will report on battery life after more use, so far it's about 1% per evening (30-45 minutes of reading, with front-light turned off).

Configurability is a very strong point of the PBL3. On the one hand, it's not overly complex, on the other -- everything I needed so far (and many things more, which I discovered while dwelling through the menus) can be customized. For example, one may customize actions of the power button action (single press, double press), its LED indicator, all front buttons (which may be key mapped to many reading-related actions).

So far, my experience with PocketBook Basic Lux 3 is very good, I'm glad I've made the switch.

*11/08/2023*

Almost one-year later update.

I still think this is the great piece of hardware. The battery life is long, performance is optimal, screen quality is fine. I particularly appreciate the physical buttons. SMARTlight is ok. It has survived many days of traveling with me. The only single complaint that I have is a small suspend/wake up button. This might be more protruding and easier to operates with gloves. Nothing else comes to my mind.

A solid 5 out of 5 from me.