# Fairphone 3+ and /e/, part 2

*16/12/2020*

It's been almost a month since I've replaced my iPhone SE with Fairphone 3+ ([FP3+](https://shop.fairphone.com/en/fairphone-3-plus)) running [/e/](https://e.foundation/) (degoogled Android). I still use it and not even consider going back to the Apple iOS ecosystem.

First, the phone is really fine. While initially I thought its display size is a little to big for my, I find it much better to my eyes than the smaller iPhone. There were no technical problem with the phone. The battery life is great, I get 2 days of regular use (calls, messages, email, browsing). I do not game at all (with the exception of [DroidFish](https://github.com/peterosterlund2/droidfish)) and do not watch lots of movies, therefore my battery life may not be of a typical mobile user.

Regarding the /e/ system, it's good but there are some issues that need improvements. First, the app store. Almost all my apps were already available and you can submit requests for missing apps. The developers add them pretty quickly (my submits were resolved within a few days). The missing thing is that you cannot uninstall apps from the store (the feature in on the road-map, though).

I like configurability of the system so much better than on the iOS. But, there are also many inconsistencies, especially with the dark mode, which is enabled for the system, but it's not global and some apps do not implement it at all and some require enabling it additionally per app. I miss the ability to replace the system mail and browser apps with Firefox and Tutanota, which I use. Currently you cannot uninstall core apps like these, the feature is on the road-map as well.

After the last upgrade (0.13-2020121490323) the system performance is very good, on the 0.12 version there were some lags, e.g. in the Mastodon app ([Tusky](https://tusky.app/)). Now the system is much smoother, apps start quickly and are quite stable. I had few crashes of some system / user-space apps, but these were really infrequent. There are some minor bugs in the system, ~~e.g. I cannot switch the haptic vibration off (the system keyboard ignores the system setting)~~. This is not a problem to me as I use the AnySoftKeyboard, which works fine. UPDATE: after the system update haptic vibration can be turned off and I've returned to the native /e/ keyboard.

The camera is really good, especially with the [OpenCamera](https://opencamera.sourceforge.io/) app, which gives me lots of tweakings (RAWs, bracketing, auto/manual focus, focus lock, etc.). This is an excellent app, much better than anything that I've tested on the iOS.

So, the switch was a successful one, I really enjoy being almost (MacBook Air, I'm looking at you) Apple-free and there is no single thing that I miss from the system or hardware.