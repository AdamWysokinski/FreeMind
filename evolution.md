# Evolution of hardware and software that I use

UpdateL ~~*04/05/2021*~~ *11/08/2023*

Over the last three decades the evolution of computer platforms and software was sometimes circular. Wherever possible, I tend to use open software, although pragmatically I accept proprietary alternatives if they are significantly better. However, I completely avoid subscription based software and services (e.g. Netflix, Spotify), with three exceptions: Tutanota (best privacy-focused mail provider), Windy and Plotaroute (which I use for my trekking and bikepacking activities). Actually, there are only four proprietary programs that I use regularly (Sublime Text, Sublime Merge, Bitwig Studio and REAPER). I also keep my Stata and MATLAB licenses going, as I need these from time to time. For generative 3D graphics I also did some work using SideFX Houdini, but its price is prohibitive for a hobbyist like me.

Computers:

Atari 800 XL (80s) → Commodore Amiga 500 (90s) → Commodore Amiga 1200 (90s) → PC (AMD Athlon) (90s-00s) → various Macs (MacBook Pro/iMac/Mac Mini, MacBook Air) (00s-20s) → PC (AMD Threadripper 2) and ThinkPad T14

OSes:

Amiga OS (90s) → Windows (90s-00s) → Linux (briefly, SuSE, PLD, Gentoo) → Windows (XP, 2000) → MacOS (from Tiger to Mojave) → Linux (Debian Stable → Testing) → FreeBSD (briefly, I really like the idea but don't have time to implement it for my needs) → Linux (Debian Testing, again) → Linux (Arch, briefly) → Linux (Debian Testing, again)

Programming languages:

- Atari Basic → AMOS Basic → Pascal → Delphi → C# → Python and Julia → Julia and zsh
- Stata (I still infrequently use it when working with my PhD-students) → R → Julia
- Octave (tried, but could not make the EEGLAB to work properly) → MATLAB (mostly for EEGLAB) → Julia

Text editors:

BBEdit → Vim → Neovim → Emacs → Sublime Text → Neovim → Sublime Text

Research papers writing:

Word + EndNote → Word + Bookends → Emacs (org-mode + org-ref) → LibreOffice + Zotero → Sublime Text (Markdown) + JabRef + Pandoc

I don't consider the current combination final, as I like to explore new ideas and approaches. However, my last attempt to move from Linux to FreeBSD and return to Neovim showed that my level of satisfaction with the current setup is very high. Also, as much as I adore the idea of open and free software, some open/free alternatives are simply not up to par (e.g. MATLAB which is so better compared with Octave in terms of speed, graphics and documentation or Sublime Text, which clicked with me so much that an attempt of going back to Neovim was like a torture to me).
